package com.sim.news.simplenews.model;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class NewsItemResponse {

    public static List<NewsItem> parsefromJSON(JSONObject respose){
        List<NewsItem> items = new ArrayList<>();
        try {
            JSONObject item0 = respose.getJSONObject("newsItem0");
            items.add(parseNewsItem(item0));
            JSONObject item1 = respose.getJSONObject("newsItem1");
            items.add(parseNewsItem(item1));
            JSONObject item2 = respose.getJSONObject("newsItem2");
            items.add(parseNewsItem(item2));
            JSONObject item3 = respose.getJSONObject("newsItem3");
            items.add(parseNewsItem(item3));

        } catch (JSONException exception){
            //TODO handle parsing error
        }
        return items;
    }

    private static NewsItem parseNewsItem(JSONObject item) throws JSONException{
        NewsItem newsItem = new NewsItem();
        newsItem.title = item.getString("title");
        newsItem.text0 = item.getString("text0");
        newsItem.text1 = item.getString("text1");
        newsItem.image0 = item.getString("image0");
        newsItem.image1 = item.getString("image1");
        return newsItem;

    }
}
