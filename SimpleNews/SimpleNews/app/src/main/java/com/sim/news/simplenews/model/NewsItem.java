package com.sim.news.simplenews.model;

import java.io.Serializable;

public class NewsItem implements Serializable {

    public String title;
    public String text0;
    public String text1;
    public String image0;
    public String image1;

}
