package com.sim.news.simplenews.server;

import android.content.AsyncTaskLoader;
import android.content.Context;
import android.content.Loader;

import com.sim.news.simplenews.model.NewsItem;
import com.sim.news.simplenews.model.NewsItemResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class SimpleNewsLoader extends AsyncTaskLoader<List<NewsItem>> {

    private OkHttpClient mHTTPClient;
    private static final String sURL = "https://s3.amazonaws.com/mobileapps-experimental/dummy.json";
    private List<NewsItem> mData;

    public SimpleNewsLoader(Context context) {
        super(context);
        mHTTPClient = new OkHttpClient();
    }

    @Override
    public List<NewsItem> loadInBackground() {
        List<NewsItem> data = new ArrayList<>();
        try {

            Request request = new Request.Builder()
                    .url(sURL)
                    .build();

            Response response = mHTTPClient.newCall(request).execute();
            data = NewsItemResponse.parsefromJSON(new JSONObject(response.body().toString()));
        } catch (IOException ioException){
            //TODO Handle error
        } catch (JSONException jsonException){
            //TODO Handle json parsing exception
        }
        return data;
    }

    @Override
    protected void onStartLoading() {
        if (mData != null) {
            // Deliver any previously loaded data immediately.
            deliverResult(mData);
        }

    }

    @Override
    public void deliverResult(List<NewsItem> data) {
        super.deliverResult(data);
        mData = data;
    }

    @Override
    protected void onStopLoading() {

    }
}
