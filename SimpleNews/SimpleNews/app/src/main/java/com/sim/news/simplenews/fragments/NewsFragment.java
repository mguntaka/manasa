package com.sim.news.simplenews.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.sim.news.simplenews.R;
import com.sim.news.simplenews.model.NewsItem;

import java.util.Random;

import butterknife.Bind;
import butterknife.ButterKnife;

public class NewsFragment extends Fragment {

    public static final String ARG_NEW_ITEM = "new_item";


    public static final NewsFragment newInstance(NewsItem item) {
        NewsFragment fragment = new NewsFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_NEW_ITEM, item);
        fragment.setArguments(args);
        return fragment;
    }

    private NewsItem mNewsItem;
    private int max = 4;
    private int min = 1;

    @Bind(R.id.image0)
    ImageView mImage0;
    @Bind(R.id.title)
    TextView mTitle;
    @Bind(R.id.text0)
    TextView mTextView0;
    @Bind(R.id.image1)
    ImageView mImage1;
    @Bind(R.id.text1)
    TextView mTextView1;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mNewsItem = (NewsItem) getArguments().getSerializable(ARG_NEW_ITEM);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(getRandomLayout(), null);
        ButterKnife.bind(this, root);

        return root;
    }

    private int getRandomLayout(){
        int randomNumber = new Random().nextInt(max - min + 1) + min;
        int layoutId;
        switch (randomNumber){
            case 1:
                layoutId = R.layout.fragment_news_item1;
                break;
            case 2:
                layoutId = R.layout.fragment_news_item2;
                break;
            case 3:
                layoutId = R.layout.fragment_news_item3;
                break;
            case 4:
                layoutId = R.layout.fragment_news_item4;
                break;
            default:
                layoutId = R.layout.fragment_news_item5;
                break;
        }
        return layoutId;

    }

    @Override
    public void onResume() {
        super.onResume();

        if (mNewsItem != null) {
            Glide.with(this)
                    .load(mNewsItem.image0)
                    .centerCrop()
                    .into(mImage0);
            Glide.with(this)
                    .load(mNewsItem.image1)
                    .centerCrop()
                    .into(mImage1);

            mTitle.setText(mNewsItem.title);

            mTextView0.setText(mNewsItem.text0);
            mTextView1.setText(mNewsItem.text1);
        }
    }
}
