package com.sim.news.simplenews.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.sim.news.simplenews.R;

import butterknife.Bind;
import butterknife.ButterKnife;

public class SettingsActivity extends AppCompatActivity{

    public static final String SETTINGS_SHARED_PREFS = "MyPrefs" ;

    public static Intent newIntent(Context context) {
        return new Intent(context, SettingsActivity.class);
    }

    @Bind(R.id.save_mode)
    CheckBox mSaveCurrentMode;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        ButterKnife.bind(this);

        SharedPreferences sharedPref = getSharedPreferences(SETTINGS_SHARED_PREFS, Context.MODE_PRIVATE);
        boolean isSaveCurrentModeOn = sharedPref.getBoolean(getString(R.string.save_mode), false);
        mSaveCurrentMode.setChecked(isSaveCurrentModeOn);
        mSaveCurrentMode.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SharedPreferences sharedPref = getSharedPreferences(SETTINGS_SHARED_PREFS, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(getString(R.string.save_mode), isChecked);
                editor.commit();
            }
        });
    }
}
