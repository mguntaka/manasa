package com.sim.news.simplenews.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.sim.news.simplenews.R;
import com.sim.news.simplenews.fragments.NewsFragment;
import com.sim.news.simplenews.model.NewsItem;
import com.sim.news.simplenews.model.NewsItemResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class NewsActivity extends AppCompatActivity {

    public static Intent newIntent(Context context) {
        return new Intent(context, NewsActivity.class);
    }

    private NewsFragmentPager mViewPagerAdapter;
    private Handler mTimerHandler;
    private boolean mSaveCurrentMode;
    private static final int DELAY_TIME_IN_MS = 3000;
    @Bind(R.id.view_pager) ViewPager mViewPager;
    @Bind(R.id.periodic_mode) CheckBox mPeriodicMode;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);
        ButterKnife.bind(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("debug","onResume()");

        new DownloadNewsFeed().execute();

        mTimerHandler = new Handler();

        mPeriodicMode.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                SharedPreferences sharedPref = getSharedPreferences(SettingsActivity.SETTINGS_SHARED_PREFS, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(getString(R.string.periodic_mode), isChecked);
                editor.commit();
                Log.d("debug","mPeriodicMode.setOnCheckedChange()... isChecked = "+isChecked);

                if(isChecked){
                    mTimerHandler.postDelayed(mRunnable, DELAY_TIME_IN_MS);
                } else {
                    mTimerHandler.removeCallbacks(mRunnable);
                }
            }
        });

        SharedPreferences sharedPref = getSharedPreferences(SettingsActivity.SETTINGS_SHARED_PREFS, Context.MODE_PRIVATE);
        mSaveCurrentMode = sharedPref.getBoolean(getString(R.string.save_mode), false);
        boolean periodicModeValue;
        if (mSaveCurrentMode) {
            periodicModeValue = sharedPref.getBoolean(getString(R.string.periodic_mode), true);
        } else {
            periodicModeValue = true;
        }
        Log.d("debug","mSaveCurrentMode = "+mSaveCurrentMode +" PeriodicModeValue = "+periodicModeValue);

        mPeriodicMode.setChecked(periodicModeValue);
        if(periodicModeValue) {
            mTimerHandler.postDelayed(mRunnable, DELAY_TIME_IN_MS);
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("debug","onResume()");
        mTimerHandler.removeCallbacks(mRunnable);
        mTimerHandler = null;
        mRunnable = null;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_settings:
                startActivity(SettingsActivity.newIntent(this));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            Log.d("debug","mRunnable.run() mViewPagerAdapter.getCount() = "+mViewPagerAdapter.getCount() +"...mViewPager.getCurrentItem() = "+mViewPager.getCurrentItem());

            if (mViewPagerAdapter.getCount() > 0){
                int currentPosition = mViewPager.getCurrentItem();
                if(currentPosition < (mViewPagerAdapter.getCount()-1)) {
                    mViewPager.setCurrentItem(currentPosition + 1);
                } else {
                    mViewPager.setCurrentItem(0);
                }
                if(mPeriodicMode.isChecked()) {
                    mTimerHandler.postDelayed(mRunnable, DELAY_TIME_IN_MS);
                }
            }
        }
    };

    private class DownloadNewsFeed extends AsyncTask<Void, Void, List<NewsItem>> {
        @Override
        protected List<NewsItem> doInBackground(Void... params) {
            List<NewsItem> data = new ArrayList<>();
            OkHttpClient httpClient = new OkHttpClient();
            try {

                Request request = new Request.Builder()
                        .url(getString(R.string.config_url))
                        .addHeader("Content-Type", "application/json")
                        .build();

                Response response = httpClient.newCall(request).execute();

                InputStream in = response.body().byteStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                String result, line = reader.readLine();
                result = line;
                while((line = reader.readLine()) != null) {
                    result += line;
                }
                response.body().close();
                data = NewsItemResponse.parsefromJSON(new JSONObject(result));
            } catch (IOException ioException){
                //TODO Handle error
            } catch (JSONException jsonException){
                //TODO Handle json parsing exception
            }
            return data;
        }

        @Override
        protected void onPostExecute(List<NewsItem> newsItems) {
            super.onPostExecute(newsItems);
            mViewPagerAdapter = new NewsFragmentPager(getSupportFragmentManager(), newsItems);
            mViewPager.setAdapter(mViewPagerAdapter);
        }
    }

    public static class NewsFragmentPager extends FragmentPagerAdapter {
        private List<NewsItem> mNewsItems;

        public NewsFragmentPager(FragmentManager fragmentManager, List<NewsItem> newsItems) {
            super(fragmentManager);
            mNewsItems = newsItems;
        }

        @Override
        public int getCount() {
            return mNewsItems.size();
        }

        @Override
        public Fragment getItem(int position) {
            return NewsFragment.newInstance( mNewsItems.get(position));
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mNewsItems.get(position).title;
        }

    }

}
